//Core module of node.js
//const http = require('http');
const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const errorController = require('./controllers/error');
const sequelize = require('./util/database');

const Product = require('./models/product');
const User = require('./models/user');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(bodyParser.urlencoded({extended: false}));

// static is a built-in middleware fxn in express that helps to sereve static files
// so in public folder we give those file to the client they have read only access
app.use(express.static(path.join(__dirname, 'public')));

app.use('/admin', adminRoutes);
app.use(shopRoutes);

// This piece of code run only when it did not find any of the route in above defining routes 
// app.use() will execute whether its a get, post or any request
app.use(errorController.get404 )

sequelize
    .sync()
    .then(result => {
        app.listen(3000);
    })
    .catch(err => {
        console.log(err);
    });



// This code will always excute bcz it is placed on top and match all the route and rest of the route is also bcz we are using next() in the end
// app.use('/' ,(req, res, next) => {
//     console.log('this always run');
//     next();
// });

// app.use('/add' ,(req, res, next) => {
//     console.log('in add middleware');
//     res.send('<h1> add page <?h1>');
// });

// If we keep this above all the route then these line of code always execute and rest of the will never excute 
// Bcz every route starts with '/' so this condition always meet and we are using next() at the end of this callback

// app.use('/' ,(req, res, next) => {
//     console.log('in another middleware');
//     res.send('<h1> main page <?h1>');
// });

// The order of defining the route is imp

// app.listen(3000);
// Above line do the same thing as below these two lines are 
// const server = http.createServer(app);
// server.listen(3000);