const Product = require('../models/product');

exports.getProducts = (req, res, next) => {
  Product.findAll()
  .then( products =>
    res.render('shop/product-list', {
      prods: products,
      pageTitle: 'All Products',
      path: '/products'
      }))
  .catch(err => console.log(err));
};

exports.getProductDetails = (req, res, next) => {
  const prodId = req.params.productId;
  Product.findByPk(prodId)
  .then(product => {
    res.render('shop/product-detail', {
    pageTitle: product.title,
    path: '/products',
    product: product,
    })
  })
  .catch(err => console.log(err));
}  

exports.getIndex = (req, res, next) => {
  Product.findAll()
    .then(products =>
      res.render('shop/index', {
      prods: products,
      pageTitle: 'Shop',
      path: '/'
    }))
    .catch(err => console.log(err));
};

exports.postCart = (req, res, next) => {
  const prodId = req.body.productId;
    res.render('shop/cart', {
    path: '/cart',
    pageTitle: 'Your Cart'
  })
}

exports.getCart = (req, res, next) => {
    res.render('shop/cart', {
        path: '/cart',
        pageTitle: 'Your Cart'
    })
};

exports.getCheckout = (req, res, next) =>{
    res.render('shop/checkout', {
        path: '/checkout',
        pageTitle: 'Checkout'
    })
}

exports.getOrders = (req, res, next) => {
    res.render('shop/orders', {
        pageTitle: 'Your Orders',
        path: '/orders'
    });
}