const path = require('path');

const express = require('express');
const router = express.Router();

const adminController = require('../controllers/admin');

router.get('/add-product', adminController.getAddProduct);

router.post('/add-product', adminController.postAddProduct);

router.get('/products', adminController.getProducts);

router.get('/edit-product/:productId', adminController.getEditProduct);

router.post('/edit-product', adminController.postEditProduct);

router.post('/delete-product', adminController.postDeleteProduct);

module.exports = router;

//Using templating engine PUG to render the view 
//res.render('add-product', {pageTitle: 'Add Product', path: '/admin/add-product'})

//Way to include HTML file but we cant deal with dynamic content like passing data from this file to html
//res.sendFile(path.join(rootDir, 'views', 'add-product.html'));
