const path = require('path');
const express = require('express');

const router = express.Router();

const shopController = require('../controllers/shop');
const rootDir = require('../util/path'); 

router.get('/', shopController.getIndex);

router.get('/products', shopController.getProducts);

//By using colon we can define params in route. It matches all the route with /products/890
router.get('/products/:productId', shopController.getProductDetails);

router.get('/cart', shopController.getCart);

router.post('/cart', shopController.postCart);

router.get('/checkout', shopController.getCheckout);

router.get('/orders', shopController.getOrders);

module.exports = router;

//res.sendFile(path.join(rootDir ,"views", "shop.html"));
//res.render('shop', {prods: adminData.products, pageTitle: 'Shop', path:'/'});
//Here routes.get() or routes.post do the exact match so every routes will not end up here like in the case of routes.use('/') this will match the every request if it is get or post, /add-product